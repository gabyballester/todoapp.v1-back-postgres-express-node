-- Script de creación de la base de datos
CREATE DATABASE perntodo;

CREATE TABLE todo(
  todo_id SERIAL PRIMARY KEY, -- será único
  description VARCHAR(255)
);